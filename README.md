# Ansible role vault unsealer

Run _vault operator unseal {{unseal_key}}_ on a vault container deployed on a docker host

## Requirements

Assumes vault running on docker

## Role Variables

```yaml
# vault container name
container: infra_secrets
# vault unseal key to be injected as a playbook variable
unseal_key: to_change
```

## Example Playbook

```yaml
- hosts: secrets_tasks_hosts
  tasks:
    - import_role:
      name: unsealer
      vars:
        container: infra_secrets
```

## Example script

```shell
#!/bin/sh
stty -echo
echo Enter one unseal key :
read unseal_key
stty echo
ansible-playbook -i inventories/production unsealer.yml --extra-vars "unseal_key=$unseal_key"
```

## License

MIT
